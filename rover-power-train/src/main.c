/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <console/console.h>
#include <string.h>
#include <stdio.h>
#include <drivers/uart.h>
#include <stdlib.h>

//#include <board.h>

/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS   5000

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0	DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN	DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS	DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0	""
#define PIN	0
#define FLAGS	0
#endif

#define PORTA	"GPIOA"
#define PORTB	"GPIOB"

#define CONTROL_SERIAL DT_NODELABEL(usart3)


#define STARTER_PIN_B 13

const struct device *gpiob;

/* size of stack area used by each thread */
#define STACKSIZE 4096

/* scheduling priority used by each thread */
#define PRIORITY 7

struct control{
	int left_padx;
	int left_pady;
	uint16_t left_trig;
	uint16_t right_trig;
	uint8_t start;
}ctrl_in;

struct motor{
	const struct device *port;
	gpio_pin_t pinA_FWD;
	gpio_pin_t pinB_REV;
	gpio_pin_t pinPWM;
	int rpm;
	int pwm;
	bool dir;
	void (*motor_run)(const struct motor*);
};

const struct device *hrt;
const struct device *gpioa;
const struct device *gpiob;
const struct device *uar;

struct motor drive[4];

void motor_initialize()
{
	int ret;

	drive[0].port = gpiob;
	drive[0].pinA_FWD = 14;
	drive[0].pinB_REV = 15;
	drive[0].pinPWM = 1;	

	drive[1].port = gpiob;
	drive[1].pinA_FWD = 8;
	drive[1].pinB_REV = 9;
	drive[1].pinPWM = 0;

	drive[2].port = gpioa;
	drive[2].pinA_FWD = 5;
	drive[2].pinB_REV = 4;
	drive[2].pinPWM = 3;

	drive[3].port = gpioa;
	drive[3].pinA_FWD = 1;
	drive[3].pinB_REV = 0;
	drive[3].pinPWM = 2;

	for (int i = 0; i < 4; i++)
	{
		drive[i].rpm = 0;
		drive[i].pwm = 0;
		drive[i].dir = 0;

		ret = gpio_pin_configure(drive[i].port, drive[i].pinA_FWD, GPIO_OUTPUT);
		if (ret < 0) {
			return;
		}

		ret = gpio_pin_configure(drive[i].port, drive[i].pinB_REV, GPIO_OUTPUT);
		if (ret < 0) {
			return;
		}
	}

}

void motor_run(const struct motor* m)
{

	printk("test");
}

void controller_input()
{
	char recv_char;
	char recv_str_temp[30];
	uar = device_get_binding(DT_LABEL(CONTROL_SERIAL));
    if (!uar) {
		printk("UART3 init failed!\n");
		return;
	}

	while(1) {
		while(1) {
			while (uart_poll_in(uar, &recv_char) < 0)
			{
				k_yield();
			}
			sprintf(recv_str_temp + strlen(recv_str_temp), "%c", recv_char);
			if ( (recv_char == '\r'))
			{
				break;
			}
			k_yield();
		}

		sscanf(recv_str_temp, "%d\t%d\t%hu\t%hu\t%hhu\n", &ctrl_in.left_padx, &ctrl_in.left_pady, &ctrl_in.left_trig, &ctrl_in.right_trig, &ctrl_in.start);
		strcpy(recv_str_temp, "");
		printk("Left Padx: %d\nLeft Pady: %d\nLeft Trigger: %hu\nRight Trigger: %hu\nStart: %hhu\n", ctrl_in.left_padx, ctrl_in.left_pady, ctrl_in.left_trig, ctrl_in.right_trig, ctrl_in.start);
		k_yield();
	}
}

void startup()
{

	bool startup_flag = 0;
	bool key_pos = 0;
	bool key_prev_pos = 0;

	while (1)
	{
		key_pos = ctrl_in.start;
		if(key_pos == 1 && key_prev_pos == 0)
		{
			key_prev_pos = 1;
			startup_flag = !startup_flag;
			gpio_pin_set(gpiob, STARTER_PIN_B, (int)startup_flag);
		}
		else if(key_pos == 0 && key_prev_pos == 1)
		{
			key_prev_pos = 0;
		}
		
		k_yield();
	}
	
}

void main(void)
{
	bool led_is_on = true;
	int ret;

	hrt = device_get_binding(LED0);
	if (hrt == NULL) {
		return;
	}

	ret = gpio_pin_configure(hrt, PIN, GPIO_OUTPUT_ACTIVE | FLAGS);
	if (ret < 0) {
		return;
	}

	gpioa = device_get_binding(PORTA);
	if (!gpioa) {
		printk("Cannot find %s!\n", PORTA);
		return;
	}

	gpiob = device_get_binding(PORTB);
	if (!gpiob) {
		printk("Cannot find %s!\n", PORTB);
		return;
	}
	ret = gpio_pin_configure(gpiob, STARTER_PIN_B, GPIO_OUTPUT);
	if (ret < 0) {
		return;
	}





	while (1) {
		gpio_pin_set(hrt, PIN, (int)led_is_on);
		led_is_on = !led_is_on;
		printk("a\n");
		k_msleep(500);	
	}
}

K_THREAD_DEFINE(controller_input_id, 1024, controller_input, NULL, NULL, NULL, PRIORITY, 0, 0);
K_THREAD_DEFINE(startup_id, 512, startup, NULL, NULL, NULL, PRIORITY, 0, 0);